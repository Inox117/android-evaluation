package com.example.quentin.qvictoor.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.quentin.qvictoor.R;
import com.example.quentin.qvictoor.book.model.Book;

public class FragmentDetailsBook extends Fragment {

    private Book bookToDetail;
    private TextView titleView;
    private ImageView imageView;
    private TextView priceView;
    private TextView isbnView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_book, container, false);

        titleView = (TextView) view.findViewById(R.id.detailsTitle);
        isbnView = (TextView) view.findViewById(R.id.detailsIsbn);
        priceView = (TextView) view.findViewById(R.id.detailsPrice);
        imageView = (ImageView) view.findViewById(R.id.detailsImage);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(bookToDetail != null) {

            titleView.setText(bookToDetail.getTitle());
            isbnView.setText(bookToDetail.getIsbn());
            priceView.setText(bookToDetail.getPrice()+"€");
            String url = bookToDetail.getCover();
            Glide.with(this).load(url).into(imageView);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (this.getArguments() != null) {
            bookToDetail = (Book) this.getArguments().get("selectedBook");
        }
    }

}
