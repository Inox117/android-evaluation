package com.example.quentin.qvictoor.listener;

import com.example.quentin.qvictoor.book.model.Book;

/**
 * Created by quentin on 14/02/16.
 * Evalutation du cours d'Android
 * à L'Ecole des Mines de Nantes
 * Promotion FIL 2016
 */
public interface ItemClickListener {
    void onClick(Book book);
}
