package com.example.quentin.qvictoor.book.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by quentin on 24/01/16.
 * Evalutation du cours d'Android
 * à L'Ecole des Mines de Nantes
 * Promotion FIL 2016
 */
public class Book implements Parcelable {

    public final String title;
    public final float price;
    public final String isbn;
    public final String cover;

    public Book(String title, float price, String isbn, String cover) {
        this.title = title;
        this.price = price;
        this.isbn = isbn;
        this.cover = cover;
    }

    protected Book(Parcel source){
        title = source.readString();
        cover = source.readString();
        isbn = source.readString();
        price = source.readFloat();
    }

    public String getTitle() {
        return title;
    }

    public float getPrice() {
        return price;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getCover() {
        return cover;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        return Float.compare(book.price, price) == 0 && title.equals(book.title) && isbn.equals(book.isbn);

    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + (price != +0.0f ? Float.floatToIntBits(price) : 0);
        return result;
    }

    public static final Creator<Book> CREATOR =
            new Creator<Book>() {
                @Override
                public Book createFromParcel(Parcel source) {
                    return new Book(source);
                }

                @Override
                public Book[] newArray(int size) {
                    return new Book[size];
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(cover);
        dest.writeString(isbn);
        dest.writeFloat(price);
    }
}