package com.example.quentin.qvictoor.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.quentin.qvictoor.book.adapter.BookListAdapterRecycler;
import com.example.quentin.qvictoor.book.service.BookService;
import com.example.quentin.qvictoor.listener.DetailsStepListener;
import com.example.quentin.qvictoor.R;
import com.example.quentin.qvictoor.book.model.Book;
import com.example.quentin.qvictoor.listener.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class FragmentListBook extends Fragment {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    protected DetailsStepListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (DetailsStepListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_books, container, false);

        final List<Book> books = getBooks();
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(view.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new BookListAdapterRecycler(this.getContext(), books, new ItemClickListener() {
            @Override
            public void onClick(Book book) {
                listener.onNext(book);
            }
        });


        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    private List<Book> getBooks() {
        final ArrayList<Book> books = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://henri-potier.xebia.fr/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        BookService service = retrofit.create(BookService.class);
        Call<List<Book>> call = service.getBooks();
        call.enqueue(new Callback<List<Book>>() {
            @Override
            public void onResponse(Response<List<Book>> response, Retrofit retrofit) {
                books.addAll(response.body());
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
        return books;
    }

}
