package com.example.quentin.qvictoor.book.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.quentin.qvictoor.R;
import com.example.quentin.qvictoor.book.model.Book;
import com.example.quentin.qvictoor.listener.ItemClickListener;

import java.util.List;

/**
 * Created by quentin on 24/01/16.
 * Evalutation du cours d'Android
 * à L'Ecole des Mines de Nantes
 * Promotion FIL 2016
 */
public class BookListAdapterRecycler extends RecyclerView.Adapter<BookListAdapterRecycler.ViewHolder> {

    private final Context context;
    private final List<Book> books;
    private ItemClickListener listener;

    public BookListAdapterRecycler(Context context, List<Book> books, ItemClickListener listener) {
        this.context = context;
        this.books = books;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView;
        public TextView priceTextView;
        public ImageView imageView;
        public ViewHolder(View v) {
            super(v);
            titleTextView = (TextView) itemView.findViewById(R.id.nameTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.priceTextView);
            imageView = (ImageView) itemView.findViewById(R.id.listImage);
        }

        public void bind(final Book item, final ItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_book_recycler_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Book book = books.get(position);
        holder.titleTextView.setText(book.title);
        holder.priceTextView.setText(String.valueOf(book.price));
        Glide.with(context).load(book.cover).into(holder.imageView);
        holder.bind(book, listener);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

}
