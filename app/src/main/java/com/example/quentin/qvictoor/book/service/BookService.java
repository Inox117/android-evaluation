package com.example.quentin.qvictoor.book.service;

import com.example.quentin.qvictoor.book.model.Book;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by quentin on 30/01/16.
 * Evalutation du cours d'Android
 * à L'Ecole des Mines de Nantes
 * Promotion FIL 2016
 */
public interface BookService {

    @GET("books")
    Call<List<Book>> getBooks();

}
