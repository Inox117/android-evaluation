package com.example.quentin.qvictoor;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.quentin.qvictoor.book.model.Book;
import com.example.quentin.qvictoor.fragments.FragmentDetailsBook;
import com.example.quentin.qvictoor.fragments.FragmentListBook;
import com.example.quentin.qvictoor.listener.DetailsStepListener;

public class LibraryActivity extends AppCompatActivity implements DetailsStepListener {

    private boolean isLandscape;
    private Book selectedBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        isLandscape = getResources().getBoolean(R.bool.landscape);
        getSupportFragmentManager().popBackStack();

        //Si on est en mode paysage
        if (isLandscape) {
            //On affiche la liste
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_frame_layout, new FragmentListBook())
                    .commit();
            //Si on a des données sauvegardées
            if (savedInstanceState != null) {
                Book book = savedInstanceState.getParcelable("selectedBook");
                // Si on a un livre sélectionné
                if (book != null) {
                    FragmentDetailsBook bookFragment = new FragmentDetailsBook();
                    Bundle args = new Bundle();
                    args.putParcelable("selectedBook", book);
                    bookFragment.setArguments(args);

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_details_book, bookFragment)
                            .addToBackStack(null)
                            .commit();
                } else {
                    //Sinon on cache le fragment des détails
                    findViewById(R.id.fragment_details_book).setVisibility(View.INVISIBLE);
                }
            } else {
                // on le cache si on a pas de données
                findViewById(R.id.fragment_details_book).setVisibility(View.INVISIBLE);
            }
        } else {
            //Si on est en portrait
            //si on a des données sauvegardées
            if (null != savedInstanceState) {
                Book book = savedInstanceState.getParcelable("selectedBook");
                //Si on a un livre de sélectionné on l'affiche
                if (book != null) {
                    FragmentDetailsBook bookFragment = new FragmentDetailsBook();
                    Bundle args = new Bundle();
                    args.putParcelable("selectedBook", book);
                    bookFragment.setArguments(args);

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container_frame_layout, bookFragment)
                            .addToBackStack(null)
                            .commit();
                } else {
                    //Sinon on affiche la liste
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container_frame_layout, new FragmentListBook())
                            .commit();
                    findViewById(R.id.fragment_details_book).setVisibility(View.GONE);
                }
            } else {
                //Sinon on affiche la liste
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container_frame_layout, new FragmentListBook())
                        .commit();
                findViewById(R.id.fragment_details_book).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onNext(Book book) {
        selectedBook = book;

        FragmentDetailsBook bookFragment = new FragmentDetailsBook();
        Bundle args = new Bundle();
        args.putParcelable("selectedBook", book);
        bookFragment.setArguments(args);

        if (isLandscape) {
            //Si on est en format paysage
            //On affiche la liste + les détails du livre sélectionné
            findViewById(R.id.fragment_details_book).setVisibility(View.VISIBLE);
            getSupportFragmentManager().popBackStack();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_frame_layout, new FragmentListBook())
                    .commit();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_details_book, bookFragment)
                    .addToBackStack(null)
                    .commit();
        } else {
            //Si on est en format portrait
            //On affiche seulement la liste
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_frame_layout, bookFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //La sauvegarde est effacée pour éviter un parasitage en cas de changement d'orientation
        selectedBook = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("selectedBook", selectedBook);
    }
}
